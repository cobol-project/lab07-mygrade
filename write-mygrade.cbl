       IDENTIFICATION DIVISION. 
       PROGRAM-ID. AVG-GRADE.
       AUTHOR. WARAPORN.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
              SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.


             
       DATA DIVISION. 
       FILE SECTION. 
       FD GRADE-FILE.
       01 GRARE-DETAIL.
          88 END-OF-GRADE                     VALUE HIGH-VALUE.
       05 ID-SUBJECT            PIC X(6).
          05 NAME-SUBJECT       PIC X(50).
          05 CREDIT-SUBJECT     PIC 9(1).
          05 GRADE-SUBJECT      PIC X(2).

       FD AVG-FILE.
       01 AVG-DETAIL.
          05 AVG-NAME           PIC X(18).
          05 AVG-GRADE-GPA      PIC 9.999.

       WORKING-STORAGE SECTION. 
       01 SUM-CREDIT            PIC 9(3).
       01 SUB-NUM-GRADE         PIC 9(1)V9(1).
       01 SUM-GRADE-CREDIT      PIC 9(3)V9(1).
       01 SUM-GPA               PIC 9(1)V9(3).
       
       01 SUM-SCI-CREDIT        PIC 9(3).
       01 SUB-SCI-NUM-GRADE     PIC 9(1)V9(1).
       01 SUM-SCI-GRADE-CREDIT  PIC 9(3)V9(1).
       01 SUM-SCI-GPA           PIC 9(1)V9(3).

       01 SUM-CS-CREDIT         PIC 9(3).
       01 SUB-CS-NUM-GRADE      PIC 9(1)V9(1).
       01 SUM-CS-GRADE-CREDIT   PIC 9(3)V9(1).
       01 SUM-CS-GPA            PIC 9(1)V9(3).


       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT GRADE-FILE 

           PERFORM UNTIL END-OF-GRADE 
                   READ GRADE-FILE 
                   AT END
                      SET END-OF-GRADE TO TRUE
                   END-READ
                   IF NOT END-OF-GRADE THEN
                      PERFORM 001-AVG-GRADE THRU 001-EXIT
           END-PERFORM
           
           
           DISPLAY "AVG-GRADE : " SUM-GPA 
           DISPLAY "AVG-SCI-GRADE : " SUM-SCI-GPA
           DISPLAY "AVG-CS-GRADE : " SUM-CS-GPA


           CLOSE GRADE-FILE 
           PERFORM 002-WRITE-FLIE THRU 002-EXIT 
           GOBACK 
           .

       001-AVG-GRADE.
           COMPUTE SUM-CREDIT = SUM-CREDIT + CREDIT-SUBJECT 
           EVALUATE TRUE
           WHEN GRADE-SUBJECT = "A"
                MOVE 4.0 TO SUB-NUM-GRADE
           WHEN GRADE-SUBJECT = "B+"
                MOVE 3.5 TO SUB-NUM-GRADE
           WHEN GRADE-SUBJECT = "B"
                MOVE 3.0 TO SUB-NUM-GRADE
           WHEN GRADE-SUBJECT = "C+"
                MOVE 2.5 TO SUB-NUM-GRADE
           WHEN GRADE-SUBJECT = "C"
                MOVE 2.0 TO SUB-NUM-GRADE
           WHEN GRADE-SUBJECT = "D+"
                MOVE 1.5 TO SUB-NUM-GRADE
           WHEN GRADE-SUBJECT = "D"
                MOVE 1.0 TO SUB-NUM-GRADE
           END-EVALUATE 
           COMPUTE SUM-GRADE-CREDIT
              =(CREDIT-SUBJECT * SUB-NUM-GRADE) + SUM-GRADE-CREDIT  
           COMPUTE SUM-GPA = SUM-GRADE-CREDIT / SUM-CREDIT        
           
           IF ID-SUBJECT(1:1) IS EQUAL TO "3" THEN
              COMPUTE SUM-SCI-CREDIT = SUM-SCI-CREDIT + CREDIT-SUBJECT 
              COMPUTE SUM-SCI-GRADE-CREDIT
                 =(CREDIT-SUBJECT * SUB-NUM-GRADE) +
                 SUM-SCI-GRADE-CREDIT
              COMPUTE SUM-SCI-GPA = SUM-SCI-GRADE-CREDIT /
                 SUM-SCI-CREDIT 
           END-IF

           IF ID-SUBJECT(1:2) IS EQUAL TO "31" THEN
              COMPUTE SUM-CS-CREDIT = SUM-CS-CREDIT + CREDIT-SUBJECT 
              COMPUTE SUM-CS-GRADE-CREDIT
                 =(CREDIT-SUBJECT * SUB-NUM-GRADE) + SUM-CS-GRADE-CREDIT
              COMPUTE SUM-CS-GPA = SUM-CS-GRADE-CREDIT / SUM-CS-CREDIT

           END-IF

           .

       001-EXIT.
           EXIT 
           .

       002-WRITE-FLIE.
           OPEN OUTPUT AVG-FILE 
           MOVE "AVG-GRADE : " TO AVG-NAME
           MOVE SUM-GPA TO AVG-GRADE-GPA 
           WRITE AVG-DETAIL 

           MOVE "AVG-SCI-GRADE :  " TO AVG-NAME
           MOVE SUM-SCI-GPA TO AVG-GRADE-GPA 
           WRITE AVG-DETAIL 

           MOVE "AVG-CS-GRADE : " TO AVG-NAME
           MOVE SUM-CS-GPA TO AVG-GRADE-GPA 
           WRITE AVG-DETAIL 

           CLOSE AVG-FILE 
           .

       002-EXIT.
           EXIT.